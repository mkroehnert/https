// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use dotenv::dotenv;
use env_logger;
use std::env;

use rbtag::{BuildDateTime, BuildInfo};

use actix_web::middleware::Logger;
use actix_web::{App, HttpServer};

use actix_files as fs;

use rustls::internal::pemfile::{certs, pkcs8_private_keys};
use rustls::{NoClientAuth, ServerConfig};
use std::io::BufReader;
use std::fs::File;

#[derive(BuildDateTime, BuildInfo)]
struct VersionInfo;

fn rustls_server_config(cert_dir: &std::path::PathBuf) -> ServerConfig {
    let mut tls_config = ServerConfig::new(NoClientAuth::new());
    // cert file
    let mut cert_path = cert_dir.clone();
    cert_path.push("localhost.crt");
    let cert_file = &mut BufReader::new(File::open(cert_path).expect("failed to open localhost.crt"));
    let cert_chain = certs(cert_file).expect("load cert file");
    // key file
    let mut key_path = cert_dir.clone();
    key_path.push("localhost.key");
    let key_file = &mut BufReader::new(File::open(key_path).expect("failed to open localhost.key"));
    let mut keys = pkcs8_private_keys(key_file).expect("load key file");
    // set cert and key(s)
    tls_config.set_single_cert(cert_chain, keys.remove(0)).expect("set single cert");
    tls_config
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug,actix_web=info");
    env_logger::init();

    dotenv().ok();
    let bind_ip = env::var("BIND_IP").expect("BIND_IP env var must be set");
    let bind_port = env::var("BIND_PORT").expect("BIND_PORT env var must be set");
    let html_dir = env::var("HTML_DIR").unwrap_or("html".to_string());
    let mut default_cert_dir = if let Some(config_dir) = dirs::config_dir() {
        let mut dir = config_dir.clone();
        dir.push("https");
        dir
    } else {
        std::path::PathBuf::new()
    };
    default_cert_dir.push("cert");
    let cert_dir = env::var("CERT_DIR").and_then(|p| {
        Ok(std::path::PathBuf::from(p))
    }).unwrap_or(default_cert_dir);
    let worker_count = usize::from_str_radix(&env::var("ACTIX_WORKER_COUNT").unwrap_or("4".to_string()), 10).expect("ACTIX_WORKER_COUNT env var must be a positive number");

    let tls_config = rustls_server_config(&cert_dir);

    println!("Build Timestamp: {}", VersionInfo {}.get_build_timestamp());
    println!("Build Commit   : {}", VersionInfo {}.get_build_commit());
    println!("Bind IP:Port   : {}:{}", bind_ip, bind_port);
    println!("HTML dir       : {}", html_dir);
    println!("Cert dir       : {:?}", cert_dir);
    println!("Worker count   : {}", worker_count);

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(Logger::new("%a %{User-Agent}i"))
            .service(fs::Files::new("/", &html_dir).use_last_modified(true).index_file("index.html"))
    })
    .workers(worker_count)
    .bind_rustls(format!("{}:{}", bind_ip, bind_port), tls_config)?
    .run()
    .await
}
