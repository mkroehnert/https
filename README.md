# Building

cargo build [--release]

# starting

```
cd cert
./create-self-signed-cert.sh
cd ..
mkdir -p ~/.config/https
cp -r cert ~/.config/https
```

copy https binary somewhere in your path

put a .env file in your project that contains for example the following and the start https inside the directory containing the .env file

```
BIND_IP=0.0.0.0
BIND_PORT=8000

HTML_DIR=html
```

# LICENSE

MIT
